<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Seller\Infrastructure\Persistence;

use Doctrine\ORM\EntityManagerInterface;
use Promofarma\CartApi\Seller\Domain\Seller;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerRepository;

final class DoctrineSellerRepository implements SellerRepository
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Seller $seller): void
    {
        $this->entityManager->persist($seller);
        $this->entityManager->flush();
    }

    public function find(SellerId $id): ?Seller
    {
        return $this->entityManager->find(Seller::class, $id);
    }

    public function delete(Seller $seller): void
    {
        $this->entityManager->remove($seller);
        $this->entityManager->flush();
    }
}
