<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Seller\Infrastructure\Persistence;

use Promofarma\CartApi\Seller\Domain\Seller;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerRepository;

final class InMemorySellerRepository
{
    private array $sellers = [];

    public function save(Seller $seller): void
    {
        $this->sellers[$seller->id()->value()] = $seller;
    }

    public function find(SellerId $id): ?Seller
    {
        if(!isset($this->sellers[$id->value()])) {
            return null;
        }

        return $this->sellers[$id->value()];
    }

    public function delete(Seller $seller): void
    {
        if(isset($this->sellers[$seller->id()->value()])) {
            unset($this->sellers[$seller->id()->value()]);
        }
    }
}
