<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Seller\Application\Delete;

use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerNotFound;
use Promofarma\CartApi\Seller\Domain\SellerRepository;
use Promofarma\Shared\Domain\Bus\Command\CommandHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteSellerCommandHandler implements CommandHandler, MessageHandlerInterface
{
    private SellerRepository $sellerRepository;

    public function __construct(SellerRepository $sellerRepository)
    {
        $this->sellerRepository = $sellerRepository;
    }

    public function __invoke(DeleteSellerCommand $command)
    {
        $sellerToDelete = $this->sellerRepository->find(new SellerId($command->uuid()));
        if (!$sellerToDelete) {
            throw new SellerNotFound(sprintf('seller with uuid %s does not exist', $command->uuid()));
        }

        $this->sellerRepository->delete($sellerToDelete);
    }
}
