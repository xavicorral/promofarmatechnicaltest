<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Seller\Application\Delete;

use Promofarma\Shared\Domain\Bus\Command\Command;

final class DeleteSellerCommand implements Command
{
    private string $uuid;

    public function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    public function uuid(): string
    {
        return $this->uuid;
    }
}
