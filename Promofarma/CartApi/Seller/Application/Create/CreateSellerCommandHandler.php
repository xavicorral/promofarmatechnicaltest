<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Seller\Application\Create;

use Promofarma\CartApi\Seller\Domain\Seller;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerName;
use Promofarma\CartApi\Seller\Domain\SellerRepository;
use Promofarma\Shared\Domain\Bus\Command\CommandHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateSellerCommandHandler implements CommandHandler, MessageHandlerInterface
{
    private SellerRepository $sellerRepository;

    public function __construct(SellerRepository $sellerRepository)
    {
        $this->sellerRepository = $sellerRepository;
    }

    public function __invoke(CreateSellerCommand $command)
    {
        $seller = Seller::create(new SellerId($command->uuid()), new SellerName($command->name()));
        $this->sellerRepository->save($seller);
    }
}
