<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Seller\Application\Create;

use Promofarma\Shared\Domain\Bus\Command\Command;

final class CreateSellerCommand implements Command
{
    private string $uuid;
    private string $name;

    public function __construct(string $uuid, string $name)
    {
        $this->uuid = $uuid;
        $this->name = $name;
    }

    public function uuid(): string
    {
        return $this->uuid;
    }

    public function name(): string
    {
        return $this->name;
    }

}
