<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Seller\Domain;

interface SellerRepository
{
    public function save(Seller $seller): void;

    public function find(SellerId $id): ?Seller;

    public function delete(Seller $seller): void;
}