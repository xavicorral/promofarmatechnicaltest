<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Seller\Domain;

final class SellerNotFound extends \RuntimeException
{
}
