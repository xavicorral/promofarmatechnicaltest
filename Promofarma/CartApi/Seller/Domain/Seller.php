<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Seller\Domain;

use Promofarma\CartApi\Product\Domain\Product;
use Promofarma\Shared\Domain\Aggregate\AggregateRoot;

final class Seller extends AggregateRoot
{
    private SellerId $id;
    private SellerName $name;
    private $products;

    private function __construct(SellerId $id, SellerName $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->products = [];
    }

    public static function create(SellerId $id, SellerName $name)
    {
        $seller = new self($id, $name);

        $seller->record(new SellerWasCreatedDomainEvent($id->value(), $name->value()));

        return $seller;
    }

    public function id()
    {
        return $this->id;
    }

    public function name()
    {
        return $this->name;
    }

    public function products()
    {
        return $this->products;
    }

    public function addProduct(Product $product)
    {
        $this->products[] = $product;
    }

    public static function fromPrimitives(array $values)
    {
        return new self(
            new SellerId($values['id']),
            new SellerName($values['name'])
        );
    }
}
