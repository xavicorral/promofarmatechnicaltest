<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Cart\Infrastructure\Persistence;

use Promofarma\CartApi\Cart\Domain\Cart;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartRepository;

final class InMemoryCartRepository
{
    private array $Carts = [];

    public function save(Cart $Cart): void
    {
        $this->Carts[$Cart->id()->value()] = $Cart;
    }

    public function find(CartId $id): ?Cart
    {
        if(!isset($this->Carts[$id->value()])) {
            return null;
        }

        return $this->Carts[$id->value()];
    }

    public function delete(Cart $Cart): void
    {
        if(isset($this->Carts[$Cart->id()->value()])) {
            unset($this->Carts[$Cart->id()->value()]);
        }
    }
}
