<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Cart\Infrastructure\Persistence;

use Doctrine\ORM\EntityManagerInterface;
use Promofarma\CartApi\Cart\Domain\Cart;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartRepository;

final class DoctrineCartRepository implements CartRepository
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Cart $Cart): void
    {
        //var_dump($cart);die();
        $this->entityManager->persist($Cart);
        $this->entityManager->flush();
    }

    public function find(CartId $id): ?Cart
    {
        return $this->entityManager->find(Cart::class, $id);
    }

    public function delete(Cart $Cart): void
    {
        $this->entityManager->remove($Cart);
        $this->entityManager->flush();
    }
}
