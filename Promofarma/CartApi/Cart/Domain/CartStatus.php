<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Cart\Domain;

use Promofarma\Shared\Domain\ValueObject\IntValueObject;

final class CartStatus extends IntValueObject
{
    const PENDING = 1;
    const CONFIRMED = 2;
}
