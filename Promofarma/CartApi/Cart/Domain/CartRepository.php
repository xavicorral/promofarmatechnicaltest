<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Cart\Domain;

interface CartRepository
{
    public function save(Cart $Cart): void;

    public function find(CartId $id): ?Cart;

    public function delete(Cart $Cart): void;
}