<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Cart\Domain;

use Promofarma\CartApi\ProductLine\Domain\CartProductLine;
use Promofarma\Shared\Domain\Aggregate\AggregateRoot;

final class Cart extends AggregateRoot
{
    private CartId $id;
    private CartUserId $userId;
    private CartStatus $status;
    private $product_lines;

    private function __construct(CartId $id, CartUserId $user_id)
    {
        $this->id = $id;
        $this->userId = $user_id;
        $this->status = new CartStatus(CartStatus::PENDING);
        $this->product_lines = [];
    }

    public static function create(CartId $id, CartUserId $user_id)
    {
        $cart = new self($id, $user_id);

        $cart->record(new CartWasCreatedDomainEvent($id->value(), $user_id->value()));

        return $cart;
    }

    public function id()
    {
        return $this->id;
    }

    public function userId()
    {
        return $this->userId;
    }

    public function product_lines()
    {
        return $this->product_lines;
    }

    public function addProductLine(CartProductLine $product_line)
    {
        $this->product_lines[] = $product_line;
    }

    public function checkout()
    {
        $this->status = new CartStatus(CartStatus::CONFIRMED);
    }
}
