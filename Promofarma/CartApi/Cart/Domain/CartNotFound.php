<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Cart\Domain;

final class CartNotFound extends \RuntimeException
{
}
