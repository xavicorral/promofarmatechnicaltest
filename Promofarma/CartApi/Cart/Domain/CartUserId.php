<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Cart\Domain;

use Promofarma\Shared\Domain\ValueObject\NameValueObject;

final class CartUserId extends NameValueObject
{

}
