<?php

namespace Promofarma\CartApi\Cart\Domain;

use Promofarma\Shared\Domain\Bus\Event\DomainEvent;

final class CartWasCreatedDomainEvent extends DomainEvent
{
    private string $id;
    private string $user_id;

    public function __construct(string $id, string $user_id)
    {
        $this->id = $id;
        $this->user_id = $user_id;
    }

    public function toPrimitives(): array
    {
        // TODO: Implement toPrimitives() method.
    }

    public static function fromPrimitives(string $aggregateId, array $body, string $eventId, string $occurredOn): DomainEvent
    {
        // TODO: Implement fromPrimitives() method.
    }

    public static function eventName(): string
    {
        return 'cart.created';
    }
}
