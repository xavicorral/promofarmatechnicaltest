<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Cart\Application\Checkout;

use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartNotFound;
use Promofarma\CartApi\Cart\Domain\CartRepository;
use Promofarma\Shared\Domain\Bus\Command\CommandHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CheckoutCartCommandHandler implements CommandHandler, MessageHandlerInterface
{
    private CartRepository $cartRepository;

    public function __construct(CartRepository $CartRepository)
    {
        $this->cartRepository = $CartRepository;
    }

    public function __invoke(CheckoutCartCommand $command)
    {
        $cartToCheckout = $this->cartRepository->find(new CartId($command->cartUuid()));
        if (!$cartToCheckout) {
            throw new CartNotFound(sprintf('cart with uuid %s does not exist', $command->cartUuid()));
        }

        $cartToCheckout->checkout();
        $this->cartRepository->save($cartToCheckout);
    }
}
