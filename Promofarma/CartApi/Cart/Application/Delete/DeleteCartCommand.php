<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Cart\Application\Delete;

use Promofarma\Shared\Domain\Bus\Command\Command;

final class DeleteCartCommand implements Command
{
    private string $cartUuid;

    public function __construct(string $uuid)
    {
        $this->cartUuid = $uuid;
    }

    public function cartUuid(): string
    {
        return $this->cartUuid;
    }
}
