<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Cart\Application\Delete;

use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartNotFound;
use Promofarma\CartApi\Cart\Domain\CartRepository;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Product\Domain\ProductNotFound;
use Promofarma\CartApi\Product\Domain\ProductRepository;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineRepository;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerNotFound;
use Promofarma\CartApi\Seller\Domain\SellerRepository;
use Promofarma\Shared\Domain\Bus\Command\CommandHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class RemoveProductFromCartCommandHandler implements CommandHandler, MessageHandlerInterface
{
    private CartRepository $cartRepository;
    private ProductRepository $productRepository;
    private CartProductLineRepository $productLineRepository;

    public function __construct(CartRepository $cartRepository, ProductRepository $productRepository, CartProductLineRepository $productLineRepository)
    {
        $this->cartRepository = $cartRepository;
        $this->productRepository = $productRepository;
        $this->productLineRepository = $productLineRepository;
    }

    public function __invoke(RemoveProductFromCartCommand $command)
    {
        $cart = $this->cartRepository->find(new CartId($command->cartUuid()));
        if(!$cart) {
            throw new CartNotFound(sprintf('cart with uuid %s does not exist', $command->cartUuid()));
        }

        $product = $this->productRepository->find(new ProductId($command->productUuid()));
        if(!$product) {
            throw new ProductNotFound(sprintf('product with uuid %s does not exist', $command->productUuid()));
        }

        $productLine = $this->productLineRepository->find(new ProductId($product->id()->value()));
        $this->productLineRepository->delete($productLine);
    }
}
