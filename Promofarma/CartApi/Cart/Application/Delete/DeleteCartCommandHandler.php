<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Cart\Application\Delete;

use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartNotFound;
use Promofarma\CartApi\Cart\Domain\CartRepository;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineRepository;
use Promofarma\Shared\Domain\Bus\Command\CommandHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteCartCommandHandler implements CommandHandler, MessageHandlerInterface
{
    private CartRepository $cartRepository;
    private CartProductLineRepository $productLineRepository;

    public function __construct(CartRepository $CartRepository, CartProductLineRepository $productLineRepository)
    {
        $this->cartRepository = $CartRepository;
        $this->productLineRepository = $productLineRepository;
    }

    public function __invoke(DeleteCartCommand $command)
    {
        $cartToDelete = $this->cartRepository->find(new CartId($command->cartUuid()));
        if (!$cartToDelete) {
            throw new CartNotFound(sprintf('cart with uuid %s does not exist', $command->cartUuid()));
        }

        $productLines = $this->productLineRepository->findAllProductLinesByCartId(new CartId($command->cartUuid()));
        foreach ($productLines as $productLine) {
            $this->productLineRepository->delete($productLine);
        }

        $this->cartRepository->delete($cartToDelete);
    }
}
