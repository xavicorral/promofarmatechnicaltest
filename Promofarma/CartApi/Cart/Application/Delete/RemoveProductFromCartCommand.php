<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Cart\Application\Delete;

use Promofarma\Shared\Domain\Bus\Command\Command;

final class RemoveProductFromCartCommand implements Command
{
    private string $cartUuid;
    private string $productUuid;

    public function __construct(string $cartUuid, string $productUuid)
    {
        $this->cartUuid = $cartUuid;
        $this->productUuid = $productUuid;
    }

    public function cartUuid(): string
    {
        return $this->cartUuid;
    }

    public function productUuid(): string
    {
        return $this->productUuid;
    }
}
