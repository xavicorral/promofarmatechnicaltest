<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Cart\Application\Create;

use Promofarma\Shared\Domain\Bus\Command\Command;

final class AddProductToCartCommand implements Command
{
    private string $cartUuid;
    private string $cartUserId;
    private string $productUuid;

    public function __construct(string $cartUuid, string $cartUserId, string $productUuid)
    {
        $this->cartUuid = $cartUuid;
        $this->cartUserId = $cartUserId;
        $this->productUuid = $productUuid;
    }

    public function cartUuid(): string
    {
        return $this->cartUuid;
    }

    public function cartUserId(): string
    {
        return $this->cartUserId;
    }

    public function productUuid(): string
    {
        return $this->productUuid;
    }
}
