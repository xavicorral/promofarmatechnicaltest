<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Cart\Application\Create;

use Promofarma\CartApi\Cart\Domain\Cart;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartRepository;
use Promofarma\CartApi\Cart\Domain\CartUserId;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Product\Domain\ProductNotFound;
use Promofarma\CartApi\Product\Domain\ProductRepository;
use Promofarma\CartApi\ProductLine\Domain\CartProductLine;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineId;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineRepository;
use Promofarma\Shared\Domain\Bus\Command\CommandHandler;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AddProductToCartCommandHandler implements CommandHandler, MessageHandlerInterface
{
    private CartRepository $cartRepository;
    private ProductRepository $productRepository;
    private CartProductLineRepository $productLineRepository;

    public function __construct(CartRepository $cartRepository, ProductRepository $productRepository, CartProductLineRepository $productLineRepository)
    {
        $this->cartRepository = $cartRepository;
        $this->productRepository = $productRepository;
        $this->productLineRepository = $productLineRepository;
    }

    public function __invoke(AddProductToCartCommand $command)
    {
        $product = $this->productRepository->find(new ProductId($command->productUuid()));
        if(!$product) {
            throw new ProductNotFound(sprintf('product with uuid %s does not exist', $command->productUuid()));
        }

        $cart = $this->cartRepository->find(new CartId($command->cartUuid()));
        if(!$cart) {
            $cart = Cart::create(new CartId($command->cartUuid()), new CartUserId($command->cartUserId()));
            $this->cartRepository->save($cart);
        }

        $productLine = CartProductLine::create(
            new CartProductLineId(Uuid::uuid4()->toString()),
            new ProductId($product->id()->value()),
            new CartId($cart->id()->value())
        );
        $this->productLineRepository->save($productLine);
    }
}
