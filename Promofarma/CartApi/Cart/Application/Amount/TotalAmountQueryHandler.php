<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Cart\Application\Amount;

use Promofarma\CartApi\Cart\Domain\CartNotFound;
use Promofarma\CartApi\Cart\Domain\CartRepository;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineRepository;
use Promofarma\Shared\Domain\Bus\Query\QueryHandler;

final class TotalAmountQueryHandler implements QueryHandler
{
    private CartRepository $cartRepository;
    private CartProductLineRepository $productLineRepository;

    public function __construct(
        CartRepository $cartRepository,
        CartProductLineRepository $productLineRepository
    )
    {
        $this->cartRepository = $cartRepository;
        $this->productLineRepository = $productLineRepository;
    }

    public function __invoke(TotalAmountQuery $query)
    {
        $cart = $this->cartRepository->find($query->cartId());
        if(!$cart) {
            throw new CartNotFound(sprintf('cart with Uuid %s not found', $query->cartId()->value()));
        }

        $productLines = $this->productLineRepository->findAllProductLinesByCartId($cart->id());
        $totalAmount = 0;
        foreach ($productLines as $productLine) {
            $totalAmount += $productLine->amount()->value();
        }

        return $totalAmount;
    }
}
