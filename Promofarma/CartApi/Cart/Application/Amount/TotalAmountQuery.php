<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Cart\Application\Amount;

use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\Shared\Domain\Bus\Query\Query;

final class TotalAmountQuery implements Query
{
    private CartId $cartId;

    public function __construct(CartId $cartId)
    {
        $this->cartId = $cartId;
    }

    public function cartId()
    {
        return $this->cartId;
    }
}
