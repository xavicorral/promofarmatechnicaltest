<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Cart\Application\Amount;

use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartNotFound;
use Promofarma\CartApi\Cart\Domain\CartRepository;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\ProductLine\Domain\CartProductLine;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineRepository;
use Promofarma\Shared\Domain\Bus\Command\CommandHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ModifyProductLineAmountCommandHandler implements CommandHandler, MessageHandlerInterface
{
    private CartRepository $cartRepository;
    private CartProductLineRepository $productLineRepository;

    public function __construct(
        CartRepository $cartRepository,
        CartProductLineRepository $productLineRepository
    ) {
        $this->cartRepository = $cartRepository;
        $this->productLineRepository = $productLineRepository;
    }

    public function __invoke(ModifyProductLineAmountCommand $command)
    {
        $cart = $this->cartRepository->find(new CartId($command->cartId()));
        if (!$cart) {
            throw new CartNotFound(sprintf('cart with Uuid %s not found', $command->cartId()));
        }

        $productLine = $this->productLineRepository->findOneProductLineByCartIdAndProductId(new CartId($command->cartId()), new ProductId($command->productId()));
        $command->operation() === CartProductLine::INCREASE_OPERATION ? $productLine->increase() : $productLine->decrease();

        $this->productLineRepository->save($productLine);
    }
}
