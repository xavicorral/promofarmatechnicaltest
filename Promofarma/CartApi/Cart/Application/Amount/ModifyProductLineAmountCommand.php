<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Cart\Application\Amount;

class ModifyProductLineAmountCommand
{
    private string $cartId;
    private string $productId;
    private string $operation;

    public function __construct(string $cartId, string $productId, string $operation)
    {
        $this->cartId = $cartId;
        $this->productId = $productId;
        $this->operation = $operation;
    }

    public function cartId()
    {
        return $this->cartId;
    }

    public function productId()
    {
        return $this->productId;
    }

    public function operation()
    {
        return $this->operation;
    }
}
