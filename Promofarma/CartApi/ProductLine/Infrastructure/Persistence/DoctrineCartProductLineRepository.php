<?php

declare(strict_types=1);

namespace Promofarma\CartApi\ProductLine\Infrastructure\Persistence;

use Doctrine\ORM\EntityManagerInterface;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\ProductLine\Domain\CartProductLine;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineRepository;

final class DoctrineCartProductLineRepository implements CartProductLineRepository
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(CartProductLine $productLine)
    {
        $this->entityManager->persist($productLine);
        $this->entityManager->flush();
    }

    public function find(ProductId $productId)
    {
        return $this->entityManager->getRepository(CartProductLine::class)->findOneBy(['product_id.value' => $productId->value()]);
    }

    public function delete(CartProductLine $productLine)
    {
        $this->entityManager->remove($productLine);
        $this->entityManager->flush();
    }

    public function findAllProductLinesByCartId(CartId $cartId)
    {
        return $this->entityManager->getRepository(CartProductLine::class)->findBy(['cart_id.value' => $cartId->value()]);
    }

    public function findOneProductLineByCartIdAndProductId(CartId $cartId, ProductId $productId): CartProductLine
    {
        return $this->entityManager->getRepository(CartProductLine::class)->findOneBy(['cart_id.value' => $cartId->value(), 'product_id.value' => $productId->value()]);
    }
}
