<?php

declare(strict_types=1);

namespace Promofarma\CartApi\ProductLine\Domain;

use Promofarma\Shared\Domain\ValueObject\Uuid;

final class CartProductLineId extends Uuid
{

}
