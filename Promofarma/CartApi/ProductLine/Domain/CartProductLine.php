<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\ProductLine\Domain;

use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\Shared\Domain\Aggregate\AggregateRoot;

class CartProductLine extends AggregateRoot
{
    const INCREASE_OPERATION = 'increase';
    const DECREASE_OPERATION = 'decrease';

    private CartProductLineId $product_line_uuid;
    private ProductId $product_id;
    private CartId $cart_id;
    private CartProductLineAmount $amount;

    private function __construct(CartProductLineId $product_line_uuid, ProductId $product_id, CartId $cart_id)
    {
        $this->product_line_uuid = $product_line_uuid;
        $this->product_id = $product_id;
        $this->cart_id = $cart_id;
        $this->amount = new CartProductLineAmount(1);
    }

    public static function create(CartProductLineId $uuid, ProductId $product_id, CartId $cart_id)
    {
        return new self($uuid, $product_id, $cart_id);
    }

    public function amount()
    {
        return $this->amount;
    }

    public function increase()
    {
        $this->amount = new CartProductLineAmount($this->amount->value()+1);
    }

    public function decrease()
    {
        $this->amount = new CartProductLineAmount($this->amount->value()-1);
    }
}
