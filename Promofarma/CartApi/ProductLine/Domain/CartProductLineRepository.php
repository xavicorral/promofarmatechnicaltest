<?php

declare(strict_types=1);

namespace Promofarma\CartApi\ProductLine\Domain;

use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Product\Domain\ProductId;

interface CartProductLineRepository
{
    public function find(ProductId $productId);

    public function findAllProductLinesByCartId(CartId $cartId);

    public function findOneProductLineByCartIdAndProductId(CartId $cartId, ProductId $productId): CartProductLine;

    public function save(CartProductLine $productLine);

    public function delete(CartProductLine $productLine);
}