<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Product\Infrastructure\Persistence;

use Promofarma\CartApi\Product\Domain\Product;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Product\Domain\ProductRepository;

final class InMemoryProductRepository
{
    private array $Products = [];

    public function save(Product $product): void
    {
        $this->Products[$product->id()->value()] = $product;
    }

    public function find(ProductId $id): ?Product
    {
        if(!isset($this->Products[$id->value()])) {
            return null;
        }

        return $this->Products[$id->value()];
    }

    public function delete(Product $product): void
    {
        if(isset($this->Products[$product->id()->value()])) {
            unset($this->Products[$product->id()->value()]);
        }
    }
}
