<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Product\Infrastructure\Persistence;

use Doctrine\ORM\EntityManagerInterface;
use Promofarma\CartApi\Product\Domain\Product;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Product\Domain\ProductRepository;

final class DoctrineProductRepository implements ProductRepository
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Product $product): void
    {
        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }

    public function find(ProductId $id): ?Product
    {
        return $this->entityManager->find(Product::class, $id);
    }

    public function delete(Product $product): void
    {
        $this->entityManager->remove($product);
        $this->entityManager->flush();
    }
}
