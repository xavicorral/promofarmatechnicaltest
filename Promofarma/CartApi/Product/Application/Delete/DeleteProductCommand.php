<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Product\Application\Delete;

use Promofarma\Shared\Domain\Bus\Command\Command;

final class DeleteProductCommand implements Command
{
    private string $uuid;
    private string $sellerUuid;

    public function __construct(string $uuid, string $sellerUuid)
    {
        $this->uuid = $uuid;
        $this->sellerUuid = $sellerUuid;
    }

    public function uuid(): string
    {
        return $this->uuid;
    }

    public function sellerUuid(): string
    {
        return $this->sellerUuid;
    }
}
