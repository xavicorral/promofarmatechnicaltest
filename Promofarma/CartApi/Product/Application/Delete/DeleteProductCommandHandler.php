<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Product\Application\Delete;

use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Product\Domain\ProductNotFound;
use Promofarma\CartApi\Product\Domain\ProductRepository;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerNotFound;
use Promofarma\CartApi\Seller\Domain\SellerRepository;
use Promofarma\Shared\Domain\Bus\Command\CommandHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteProductCommandHandler implements CommandHandler, MessageHandlerInterface
{
    private ProductRepository $productRepository;
    private SellerRepository $sellerRepository;

    public function __construct(ProductRepository $productRepository, SellerRepository $sellerRepository)
    {
        $this->productRepository = $productRepository;
        $this->sellerRepository = $sellerRepository;
    }

    public function __invoke(DeleteProductCommand $command)
    {
        $seller = $this->sellerRepository->find(new SellerId($command->sellerUuid()));
        if(!$seller) {
            throw new SellerNotFound(sprintf('seller with uuid %s does not exist', $command->uuid()));
        }

        $productToDelete = $this->productRepository->find(new ProductId($command->uuid()));
        if (!$productToDelete) {
            throw new ProductNotFound(sprintf('product with uuid %s does not exist', $command->uuid()));
        }

        $this->productRepository->delete($productToDelete);
    }
}
