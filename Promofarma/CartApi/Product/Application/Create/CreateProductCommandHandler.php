<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Product\Application\Create;

use Promofarma\CartApi\Product\Domain\Product;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Product\Domain\ProductName;
use Promofarma\CartApi\Product\Domain\ProductRepository;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerNotFound;
use Promofarma\CartApi\Seller\Domain\SellerRepository;
use Promofarma\Shared\Domain\Bus\Command\CommandHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateProductCommandHandler implements CommandHandler, MessageHandlerInterface
{
    private ProductRepository $productRepository;
    private SellerRepository $sellerRepository;

    public function __construct(ProductRepository $productRepository, SellerRepository $sellerRepository)
    {
        $this->productRepository = $productRepository;
        $this->sellerRepository = $sellerRepository;
    }

    public function __invoke(CreateProductCommand $command)
    {
        $seller = $this->sellerRepository->find(new SellerId($command->sellerUuid()));
        if(!$seller) {
            throw new SellerNotFound(sprintf('seller with uuid %s does not exist', $command->sellerUuid()));
        }

        $product = Product::create(new ProductId($command->uuid()), new ProductName($command->name()));
        $product->addSeller($seller);

        //var_dump($product);die();
        $this->productRepository->save($product);
    }
}
