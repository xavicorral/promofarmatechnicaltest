<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Product\Application\Create;

use Promofarma\Shared\Domain\Bus\Command\Command;

final class CreateProductCommand implements Command
{
    private string $uuid;
    private string $name;
    private string $sellerUuid;

    public function __construct(string $uuid, string $name, string $sellerUuid)
    {
        $this->uuid = $uuid;
        $this->name = $name;
        $this->sellerUuid = $sellerUuid;
    }

    public function uuid(): string
    {
        return $this->uuid;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function sellerUuid(): string
    {
        return $this->sellerUuid;
    }
}
