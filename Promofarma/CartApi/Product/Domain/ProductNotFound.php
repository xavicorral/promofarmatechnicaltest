<?php

declare(strict_types=1);

namespace Promofarma\CartApi\Product\Domain;

final class ProductNotFound extends \RuntimeException
{
}
