<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Product\Domain;

interface ProductRepository
{
    public function save(Product $product): void;

    public function find(ProductId $id): ?Product;

    public function delete(Product $product): void;
}