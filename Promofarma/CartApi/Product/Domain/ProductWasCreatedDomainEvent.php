<?php

namespace Promofarma\CartApi\Product\Domain;

use Promofarma\Shared\Domain\Bus\Event\DomainEvent;

final class ProductWasCreatedDomainEvent extends DomainEvent
{
    private string $id;
    private string $name;

    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function toPrimitives(): array
    {
        // TODO: Implement toPrimitives() method.
    }

    public static function fromPrimitives(string $aggregateId, array $body, string $eventId, string $occurredOn): DomainEvent
    {
        // TODO: Implement fromPrimitives() method.
    }

    public static function eventName(): string
    {
        return 'product.created';
    }
}
