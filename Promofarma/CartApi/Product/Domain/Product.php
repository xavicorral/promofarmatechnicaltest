<?php

declare(strict_types = 1);

namespace Promofarma\CartApi\Product\Domain;

use Promofarma\CartApi\ProductLine\Domain\CartProductLine;
use Promofarma\CartApi\Seller\Domain\Seller;
use Promofarma\Shared\Domain\Aggregate\AggregateRoot;

final class Product extends AggregateRoot
{
    private ProductId $id;
    private ProductName $name;
    private $sellers;
    private $product_lines;

    private function __construct(ProductId $id, ProductName $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->sellers = [];
        $this->product_lines = [];
    }

    public static function create(ProductId $id, ProductName $name)
    {
        $product = new self($id, $name);

        $product->record(new ProductWasCreatedDomainEvent($id->value(), $name->value()));

        return $product;
    }

    public function id(): ProductId
    {
        return $this->id;
    }

    public function name(): ProductName
    {
        return $this->name;
    }

    public function sellers()
    {
        return $this->sellers;
    }

    public function addSeller(Seller $seller): void
    {
        $this->sellers[] = $seller;
    }

    public function productLines()
    {
        $this->product_lines;
    }

    public function addProductLine(CartProductLine $product_line)
    {
        $this->product_lines[] = $product_line;
    }
}
