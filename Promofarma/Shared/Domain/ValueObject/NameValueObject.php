<?php

declare(strict_types = 1);

namespace Promofarma\Shared\Domain\ValueObject;

use InvalidArgumentException;

abstract class NameValueObject extends StringValueObject
{
    public function __construct(string $value)
    {
        $this->ensureIsNotEmpty($value);

        parent::__construct($value);
    }

    private function ensureIsNotEmpty(string $value): void
    {
        if(empty($value)) {
            throw new InvalidArgumentException(sprintf('<%s> cannot have be an empty string <%s>.', static::class, $value));
        }
    }
}
