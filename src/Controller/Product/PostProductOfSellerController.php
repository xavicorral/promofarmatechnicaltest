<?php

declare(strict_types=1);

namespace App\Controller\Product;

use App\Controller\ApiController;
use Promofarma\CartApi\Product\Application\Create\CreateProductCommand;
use Promofarma\CartApi\Product\Infrastructure\Persistence\DoctrineProductRepository;
use Promofarma\CartApi\Seller\Infrastructure\Persistence\DoctrineSellerRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class PostProductOfSellerController extends ApiController
{
    public function __invoke(Request $request, DoctrineProductRepository $productRepository, DoctrineSellerRepository $sellerRepository)
    {
        try {
            $this->dispatch(new CreateProductCommand(
                $request->get('uuid'),
                $request->get('name'),
                $request->get('seller_uuid')
            ));
        } catch (\Exception $exception) {
            return new JsonResponse(
                ['message' => $exception->getMessage()],
                Response::HTTP_NOT_FOUND
            );
        }

        return new Response(
            null,
            Response::HTTP_CREATED
        );
    }
}
