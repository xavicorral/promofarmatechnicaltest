<?php

declare(strict_types = 1);

namespace App\Controller\HealthCheck;

use App\Controller\ApiController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final class HealthCheckGetController extends ApiController
{
    public function __invoke(Request $request): JsonResponse
    {
        return new JsonResponse(
            [
                'health-check' => 'ok',
            ]
        );
    }
}
