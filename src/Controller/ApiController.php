<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class ApiController extends AbstractController
{
    private MessageBusInterface $commandBus;
    private MessageBusInterface $queryBus;

    public function __construct(MessageBusInterface $commandBus, MessageBusInterface $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    public function dispatch($command)
    {
        $this->commandBus->dispatch($command)->last(HandledStamp::class);
    }

    public function ask($query)
    {
        $stamp = $this->queryBus->dispatch($query)->last(HandledStamp::class);

        return $stamp->getResult();
    }
}
