<?php

declare(strict_types=1);

namespace App\Controller\Cart;

use App\Controller\ApiController;
use Promofarma\CartApi\Cart\Application\Checkout\CheckoutCartCommand;
use Promofarma\CartApi\Cart\Infrastructure\Persistence\DoctrineCartRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class PutCheckoutCartController extends ApiController
{
    public function __invoke(
        Request $request,
        DoctrineCartRepository $cartRepository
    ) {
        try {
            $this->dispatch(new CheckoutCartCommand(
                $request->get('cart_uuid')
            ));
        } catch (\Exception $exception) {
            return new JsonResponse(
                ['message' => $exception->getMessage()],
                Response::HTTP_NOT_FOUND
            );
        }

        return new Response(
            null,
            Response::HTTP_CREATED
        );
    }
}
