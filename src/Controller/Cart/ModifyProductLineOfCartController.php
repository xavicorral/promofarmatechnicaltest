<?php

declare(strict_types=1);

namespace App\Controller\Cart;

use App\Controller\ApiController;
use Promofarma\CartApi\Cart\Application\Amount\ModifyProductLineAmountCommand;
use Promofarma\CartApi\Cart\Infrastructure\Persistence\DoctrineCartRepository;
use Promofarma\CartApi\Product\Infrastructure\Persistence\DoctrineProductRepository;
use Promofarma\CartApi\ProductLine\Infrastructure\Persistence\DoctrineCartProductLineRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ModifyProductLineOfCartController extends ApiController
{
    public function __invoke(
        Request $request,
        DoctrineCartRepository $cartRepository,
        DoctrineProductRepository $productRepository,
        DoctrineCartProductLineRepository $productLineRepository
    ) {
        try {
            $this->dispatch(new ModifyProductLineAmountCommand(
                $request->get('cart_uuid'),
                $request->get('product_uuid'),
                $request->get('operation')
            ));
        } catch (\Exception $exception) {
            return new JsonResponse(
                ['message' => $exception->getMessage()],
                Response::HTTP_NOT_FOUND
            );
        }

        return new Response(
            null,
            Response::HTTP_CREATED
        );
    }
}
