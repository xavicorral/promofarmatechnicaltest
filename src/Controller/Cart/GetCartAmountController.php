<?php

declare(strict_types=1);

namespace App\Controller\Cart;

use Promofarma\CartApi\Cart\Application\Amount\TotalAmountQuery;
use Promofarma\CartApi\Cart\Application\Amount\TotalAmountQueryHandler;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Infrastructure\Persistence\DoctrineCartRepository;
use Promofarma\CartApi\ProductLine\Infrastructure\Persistence\DoctrineCartProductLineRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class GetCartAmountController extends AbstractController
{
    public function __invoke(
        Request $request,
        DoctrineCartRepository $cartRepository,
        DoctrineCartProductLineRepository $productLineRepository
    ) {
        try {
            $command = new TotalAmountQuery(new CartId($request->get('cart_uuid')));
            $totalAmount = (new TotalAmountQueryHandler($cartRepository, $productLineRepository))->__invoke($command);
        } catch (\Exception $exception) {
            return new JsonResponse(
                ['message' => $exception->getMessage()],
                Response::HTTP_NOT_FOUND
            );
        }

        return new JsonResponse(
            ['totalAmount' => $totalAmount],
            Response::HTTP_OK
        );
    }
}
