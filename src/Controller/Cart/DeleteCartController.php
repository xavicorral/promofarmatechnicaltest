<?php

declare(strict_types=1);

namespace App\Controller\Cart;

use App\Controller\ApiController;
use Promofarma\CartApi\Cart\Application\Delete\DeleteCartCommand;
use Promofarma\CartApi\Cart\Infrastructure\Persistence\DoctrineCartRepository;
use Promofarma\CartApi\ProductLine\Infrastructure\Persistence\DoctrineCartProductLineRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class DeleteCartController extends ApiController
{
    public function __invoke(
        Request $request,
        DoctrineCartRepository $cartRepository,
        DoctrineCartProductLineRepository $productLineRepository
    ) {
        try {
            $this->dispatch(new DeleteCartCommand(
                $request->get('cart_uuid')
            ));
        } catch (\Exception $exception) {
            return new JsonResponse(
                ['message' => $exception->getMessage()],
                Response::HTTP_NOT_FOUND
            );
        }

        return new Response(
            null,
            Response::HTTP_NO_CONTENT
        );
    }
}
