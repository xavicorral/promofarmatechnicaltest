<?php

declare(strict_types=1);

namespace App\Controller\Cart;

use App\Controller\ApiController;
use Promofarma\CartApi\Cart\Application\Delete\RemoveProductFromCartCommand;
use Promofarma\CartApi\Cart\Infrastructure\Persistence\DoctrineCartRepository;
use Promofarma\CartApi\Product\Infrastructure\Persistence\DoctrineProductRepository;
use Promofarma\CartApi\ProductLine\Infrastructure\Persistence\DoctrineCartProductLineRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class DeleteRemoveProductFromCartController extends ApiController
{
    public function __invoke(
        Request $request,
        DoctrineProductRepository $productRepository,
        DoctrineCartRepository $cartRepository,
        DoctrineCartProductLineRepository $productLineRepository
    ) {
        try {
            $this->dispatch(new RemoveProductFromCartCommand(
                $request->get('cart_uuid'),
                $request->get('product_id')
            ));
        } catch (\Exception $exception) {
            return new JsonResponse(
                ['message' => $exception->getMessage()],
                Response::HTTP_NOT_FOUND
            );
        }

        return new Response(
            null,
            Response::HTTP_NO_CONTENT
        );
    }
}
