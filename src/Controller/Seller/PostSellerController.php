<?php

declare(strict_types=1);

namespace App\Controller\Seller;

use App\Controller\ApiController;
use Promofarma\CartApi\Seller\Application\Create\CreateSellerCommand;
use Promofarma\CartApi\Seller\Infrastructure\Persistence\DoctrineSellerRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class PostSellerController extends ApiController
{
    public function __invoke(Request $request, DoctrineSellerRepository $sellerRepository)
    {
        try {
            $this->dispatch(new CreateSellerCommand(
                $request->get('uuid'),
                $request->get('name')
            ));
        } catch (\Exception $exception) {
            return new JsonResponse(
                ['message' => $exception->getMessage()],
                Response::HTTP_NOT_FOUND
            );
        }

        return new Response(null, Response::HTTP_CREATED);
    }
}
