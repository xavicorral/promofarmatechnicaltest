<?php

declare(strict_types=1);

namespace App\Controller\Seller;

use App\Controller\ApiController;
use Promofarma\CartApi\Seller\Application\Delete\DeleteSellerCommand;
use Promofarma\CartApi\Seller\Infrastructure\Persistence\DoctrineSellerRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class DeleteSellerController extends ApiController
{
    public function __invoke(Request $request, DoctrineSellerRepository $sellerRepository)
    {
        try {
            $this->dispatch(new DeleteSellerCommand($request->get('uuid')));
        } catch (\Exception $exception) {
            return new JsonResponse(
                ['message' => $exception->getMessage()],
                Response::HTTP_NOT_FOUND
            );
        }

        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
