<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListTablesCommand extends Command
{
    protected static $defaultName = 'app:list-tables';

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Show DB Tables');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $stmt = $this->entityManager->getConnection()->executeQuery('show tables');
        $tables = $stmt->fetchAll();
        var_dump($tables);
    }
}
