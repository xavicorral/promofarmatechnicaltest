
Feature:
  In order to prove that the Api cart section is working
  As a user
  I want to have a cart management api

  Scenario: In order to manage a cart on the platform

    # In order to have a seller on the platform
    When I send a request with method POST to "/api/seller?uuid=add864fa-3476-45e8-807c-0ff37bc5d151&name=seller dummy"
    Then the response should be empty
    And the response status code should be 201

    # In order to have a product on the platform
    When I send a request with method POST to "/api/seller/add864fa-3476-45e8-807c-0ff37bc5d151/product?uuid=aa46556a-ed57-4855-ba8c-d338adbea34b&name=Colágeno Ana Maria Lajusticia"
    Then the response should be empty
    And the response status code should be 201

    # In order to add this product to a new cart
    When I send a request with method POST to "/api/cart/4996d801-119a-4713-ae26-9c049a968448/product?user_id=33c75088-0c30-4e9d-b274-b36e6931a974&product_id=aa46556a-ed57-4855-ba8c-d338adbea34b"
    Then the response should be empty
    And the response status code should be 201

    # In order to check that the total amount of products in my cart is 1
    When I send a request with method GET to "/api/cart/4996d801-119a-4713-ae26-9c049a968448/amount"
    Then the response status code should be 200
    And the response content should be:
    """
    {"totalAmount":1}
    """

    # In order to increase the amount of items of this product in my cart
    When I send a request with method PUT to "http://localhost:8000/api/cart/4996d801-119a-4713-ae26-9c049a968448/amountModify/aa46556a-ed57-4855-ba8c-d338adbea34b/increase"
    Then the response should be empty
    And the response status code should be 201

    # In order to check that the total amount of products in my cart is 2
    When I send a request with method GET to "/api/cart/4996d801-119a-4713-ae26-9c049a968448/amount"
    Then the response status code should be 200
    And the response content should be:
    """
    {"totalAmount":2}
    """

    # In order to decrease the amount of items of this product in my cart
    When I send a request with method PUT to "http://localhost:8000/api/cart/4996d801-119a-4713-ae26-9c049a968448/amountModify/aa46556a-ed57-4855-ba8c-d338adbea34b/decrease"
    Then the response should be empty
    And the response status code should be 201

    # In order to check that the total amount of products in my cart is 1
    When I send a request with method GET to "/api/cart/4996d801-119a-4713-ae26-9c049a968448/amount"
    Then the response status code should be 200
    And the response content should be:
    """
    {"totalAmount":1}
    """

    # In order to remove the recently added product from the cart
    When I send a request with method DELETE to "/api/cart/4996d801-119a-4713-ae26-9c049a968448/product?product_id=aa46556a-ed57-4855-ba8c-d338adbea34b"
    Then the response should be empty
    And the response status code should be 204

    # In order to check that the total amount of products in my cart is 0
    When I send a request with method GET to "/api/cart/4996d801-119a-4713-ae26-9c049a968448/amount"
    Then the response status code should be 200
    And the response content should be:
    """
    {"totalAmount":0}
    """

    # In order to delete the recently created product
    When I send a request with method DELETE to "/api/seller/add864fa-3476-45e8-807c-0ff37bc5d151/product?uuid=aa46556a-ed57-4855-ba8c-d338adbea34b"
    Then the response should be empty
    And the response status code should be 204

    # In order to checkout the recently created cart
    When I send a request with method PUT to "/api/cart/4996d801-119a-4713-ae26-9c049a968448/checkout"
    Then the response should be empty
    And the response status code should be 201

    # In order to delete the recently created cart
    When I send a request with method DELETE to "/api/cart/4996d801-119a-4713-ae26-9c049a968448"
    Then the response should be empty
    And the response status code should be 204
