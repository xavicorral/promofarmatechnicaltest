
Feature:
  In order to prove that the Api products section is working
  As a user
  I want to have a products management api

  Scenario: In order to have sellers on the platform
    When I send a request with method POST to "/api/seller?uuid=add864fa-3476-45e8-807c-0ff37bc5d151&name=seller dummy"
    Then the response should be empty
    And the response status code should be 201

    And I send a request with method POST to "/api/seller/add864fa-3476-45e8-807c-0ff37bc5d151/product?uuid=aa46556a-ed57-4855-ba8c-d338adbea34b&name=Colágeno Ana Maria Lajusticia"
    Then the response should be empty
    And the response status code should be 201

    When I send a request with method DELETE to "/api/seller/add864fa-3476-45e8-807c-0ff37bc5d151/product?uuid=aa46556a-ed57-4855-ba8c-d338adbea34b"
    Then the response should be empty
    And the response status code should be 204
