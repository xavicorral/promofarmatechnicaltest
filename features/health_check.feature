
Feature:
    In order to prove that the Api is running
    As a user
    I want to have a health check scenario

    Scenario: It receives a response from Symfony's kernel
        When I send a request with method GET to "/api/health-check"
        Then the response should be received
        And the response content should be:
        """
        {"health-check":"ok"}
        """