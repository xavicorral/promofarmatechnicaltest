<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\ProductLine\Domain;

use App\Tests\Promofarma\Shared\Domain\UuidMother;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineId;

final class CartProductLineIdMother
{
    public static function create(string $value): CartProductLineId
    {
        return new CartProductLineId($value);
    }

    public static function random(): CartProductLineId
    {
        return self::create(UuidMother::random());
    }
}
