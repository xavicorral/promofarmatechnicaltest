<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\ProductLine\Domain;

use App\Tests\Promofarma\Cart\Domain\CartIdMother;
use App\Tests\Promofarma\Product\Domain\ProductIdMother;
use App\Tests\Promofarma\ProductLine\Domain\CartProductLineIdMother;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\ProductLine\Domain\CartProductLine;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineId;

final class CartProductLineMother
{
    public static function create(CartProductLineId $product_line_uuid, ProductId $product_id, CartId $cart_id): CartProductLine
    {
        return CartProductLine::create($product_line_uuid, $product_id, $cart_id);
    }

    /*public static function fromRequest(CreateProductCommand $request): CartProductLine
    {
        return self::create(
            CartIdMother::create($request->uuid()),
            ProductNameMother::create($request->name()),
            SellerMother::random()
        );
    }*/

    public static function random(): CartProductLine
    {
        return self::create(CartProductLineIdMother::random(), ProductIdMother::random(), CartIdMother::random());
    }
}
