<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\ProductLine\Domain;

use App\Tests\Promofarma\Shared\Domain\IntegerMother;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineAmount;

final class CartProductLineAmountMother
{
    public static function create(int $value): CartProductLineAmount
    {
        return new CartProductLineAmount($value);
    }

    public static function random(): CartProductLineAmount
    {
        return self::create(IntegerMother::random());
    }
}
