<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Seller\Application\Create;

use App\Tests\Promofarma\Seller\Domain\SellerIdMother;
use App\Tests\Promofarma\Seller\Domain\SellerNameMother;
use Promofarma\CartApi\Seller\Application\Create\CreateSellerCommand;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerName;

final class CreateSellerCommandMother
{
    public static function create(SellerId $id, SellerName $name): CreateSellerCommand
    {
        return new CreateSellerCommand($id->value(), $name->value());
    }

    public static function random(): CreateSellerCommand
    {
        return self::create(SellerIdMother::random(), SellerNameMother::random());
    }
}
