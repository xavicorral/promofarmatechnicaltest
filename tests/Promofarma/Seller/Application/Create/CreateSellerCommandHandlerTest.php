<?php

namespace App\Tests\Promofarma\Seller\Application\Create;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Promofarma\CartApi\Seller\Application\Create\CreateSellerCommandHandler;
use Promofarma\CartApi\Seller\Domain\Seller;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerName;
use Promofarma\CartApi\Seller\Domain\SellerRepository;

class CreateSellerCommandHandlerTest extends TestCase
{
    private CreateSellerCommandHandler $handler;
    private SellerRepository $sellerRepository;

    public function setUp(): void
    {
        $this->sellerRepository = $this->createMock(SellerRepository::class);
        $this->handler = new CreateSellerCommandHandler($this->sellerRepository);
    }

    /** @test */
    public function itShouldThrowAnExceptionIfSellerNameIsEmpty()
    {
        $this->expectException(InvalidArgumentException::class);
        $sellerName = new SellerName('');
    }
    
    /** @test */
    public function itShouldThrowAnExceptionIfSellerUuidIsNotValid()
    {
        $this->expectException(InvalidArgumentException::class);
        $sellerId = new SellerId('3fe1bce8-7282-467a-872fd7-b876fe97ad4b');
    }


    /** @test */
    public function itShouldThrowAnExceptionWhenTryingToDeleteANonExistingSeller()
    {
        $command = CreateSellerCommandMother::random();

        $this->sellerRepository
            ->expects($this->once())
            ->method('save')
            ->with($this->isInstanceOf(Seller::class))
            ->willReturn(null);

        $this->handler->__invoke($command);
    }
}
