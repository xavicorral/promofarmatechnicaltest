<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Seller\Application\Delete;

use App\Tests\Promofarma\Seller\Domain\SellerIdMother;
use App\Tests\Promofarma\Seller\Domain\SellerNameMother;
use Promofarma\CartApi\Seller\Application\Delete\DeleteSellerCommand;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerName;

final class DeleteSellerCommandMother
{
    public static function create(SellerId $id, SellerName $name): DeleteSellerCommand
    {
        return new DeleteSellerCommand($id->value(), $name->value());
    }

    public static function random(): DeleteSellerCommand
    {
        return self::create(SellerIdMother::random(), SellerNameMother::random());
    }
}
