<?php

namespace App\Tests\Promofarma\Seller\Application\Create;

use App\Tests\Promofarma\Seller\Application\Delete\DeleteSellerCommandMother;
use App\Tests\Promofarma\Seller\Domain\SellerMother;
use PHPUnit\Framework\TestCase;
use Promofarma\CartApi\Seller\Application\Create\CreateSellerCommandHandler;
use Promofarma\CartApi\Seller\Application\Delete\DeleteSellerCommandHandler;
use Promofarma\CartApi\Seller\Domain\Seller;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerName;
use Promofarma\CartApi\Seller\Domain\SellerNotFound;
use Promofarma\CartApi\Seller\Domain\SellerRepository;

class DeleteSellerCommandHandlerTest extends TestCase
{
    private DeleteSellerCommandHandler $handler;
    private SellerRepository $sellerRepository;

    public function setUp(): void
    {
        $this->sellerRepository = $this->createMock(SellerRepository::class);
        $this->handler = new DeleteSellerCommandHandler($this->sellerRepository);
    }

    /** @test */
    public function itShouldThrownAnExceptionWhenDeletingASellerThatIsNotFound()
    {
        $this->expectException(SellerNotFound::class);
        $command = DeleteSellerCommandMother::random();

        $this->sellerRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(SellerId::class))
            ->willThrowException(new SellerNotFound());

        $this->handler->__invoke($command);
    }

    /** @test */
    public function itShouldDeleteASeller()
    {
        $deleteCommand = DeleteSellerCommandMother::random();
        $seller = SellerMother::random();

        $this->sellerRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(SellerId::class))
            ->willReturn($seller);

        $this->sellerRepository
            ->expects($this->once())
            ->method('delete')
            ->with($this->isInstanceOf(Seller::class))
            ->willReturn(null);

        $this->handler->__invoke($deleteCommand);
    }
}
