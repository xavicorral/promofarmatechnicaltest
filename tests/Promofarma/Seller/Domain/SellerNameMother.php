<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Seller\Domain;

use App\Tests\Promofarma\Shared\Domain\WordMother;
use Promofarma\CartApi\Seller\Domain\SellerName;

final class SellerNameMother
{
    public static function create(string $value): SellerName
    {
        return new SellerName($value);
    }

    public static function random(): SellerName
    {
        return self::create(WordMother::random());
    }
}
