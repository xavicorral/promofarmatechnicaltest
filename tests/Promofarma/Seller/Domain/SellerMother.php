<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Seller\Domain;

use Promofarma\CartApi\Seller\Application\Create\CreateSellerCommand;
use Promofarma\CartApi\Seller\Domain\Seller;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerName;

final class SellerMother
{
    public static function create(SellerId $id, SellerName $name): Seller
    {
        return Seller::create($id, $name);
    }

    public static function fromRequest(CreateSellerCommand $request): Seller
    {
        return self::create(
            SellerIdMother::create($request->uuid()),
            SellerNameMother::create($request->name()),
        );
    }

    public static function random(): Seller
    {
        return self::create(SellerIdMother::random(), SellerNameMother::random());
    }
}
