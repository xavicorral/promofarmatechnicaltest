<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Seller\Domain;

use App\Tests\Promofarma\Shared\Domain\UuidMother;
use Promofarma\CartApi\Seller\Domain\SellerId;

final class SellerIdMother
{
    public static function create(string $value): SellerId
    {
        return new SellerId($value);
    }

    public static function random(): SellerId
    {
        return self::create(UuidMother::random());
    }
}
