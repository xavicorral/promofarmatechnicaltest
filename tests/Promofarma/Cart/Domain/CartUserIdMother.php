<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Cart\Domain;

use App\Tests\Promofarma\Shared\Domain\WordMother;
use Promofarma\CartApi\Cart\Domain\CartUserId;

final class CartUserIdMother
{
    public static function create(string $value): CartUserId
    {
        return new CartUserId($value);
    }

    public static function random(): CartUserId
    {
        return self::create(WordMother::random());
    }
}
