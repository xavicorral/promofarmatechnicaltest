<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Cart\Domain;

use App\Tests\Promofarma\Seller\Domain\SellerMother;
use Promofarma\CartApi\Cart\Application\Create\AddProductToCartCommand;
use Promofarma\CartApi\Cart\Application\Create\CreateCartCommand;
use Promofarma\CartApi\Cart\Domain\Cart;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartName;
use Promofarma\CartApi\Cart\Domain\CartUserId;
use Promofarma\CartApi\Seller\Domain\Seller;

final class CartMother
{
    public static function create(CartId $id, CartUserId $name): Cart
    {
        $cart = Cart::create($id, $name);

        return $cart;
    }

    public static function fromRequest(AddProductToCartCommand $request): Cart
    {
        return self::create(
            CartIdMother::create($request->uuid()),
            CartUserId::create($request->name())
        );
    }

    public static function random(): Cart
    {
        return self::create(CartIdMother::random(), CartUserIdMother::random());
    }
}
