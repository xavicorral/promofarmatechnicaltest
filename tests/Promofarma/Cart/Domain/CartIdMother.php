<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Cart\Domain;

use App\Tests\Promofarma\Shared\Domain\UuidMother;
use Promofarma\CartApi\Cart\Domain\CartId;

final class CartIdMother
{
    public static function create(string $value): CartId
    {
        return new CartId($value);
    }

    public static function random(): CartId
    {
        return self::create(UuidMother::random());
    }
}
