<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Cart\Application\Delete;

use App\Tests\Promofarma\Cart\Domain\CartIdMother;
use App\Tests\Promofarma\Product\Domain\ProductIdMother;
use Promofarma\CartApi\Cart\Application\Delete\RemoveProductFromCartCommand;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Product\Domain\ProductId;

final class RemoveProductFromCartCommandMother
{
    public static function create(CartId $id, ProductId $productId): RemoveProductFromCartCommand
    {
        return new RemoveProductFromCartCommand($id->value(), $productId->value());
    }

    public static function random(): RemoveProductFromCartCommand
    {
        return self::create(CartIdMother::random(), ProductIdMother::random());
    }
}
