<?php

namespace App\Tests\Promofarma\Product\Application\Create;

use App\Tests\Promofarma\Cart\Application\Delete\RemoveProductFromCartCommandMother;
use App\Tests\Promofarma\Cart\Domain\CartMother;
use App\Tests\Promofarma\Product\Domain\ProductMother;
use App\Tests\Promofarma\ProductLine\Domain\CartProductLineMother;
use App\Tests\Promofarma\Seller\Domain\SellerMother;
use PHPUnit\Framework\TestCase;
use Promofarma\CartApi\Cart\Application\Delete\RemoveProductFromCartCommandHandler;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartNotFound;
use Promofarma\CartApi\Cart\Domain\CartRepository;
use Promofarma\CartApi\Product\Application\Delete\DeleteProductCommandHandler;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Product\Domain\ProductNotFound;
use Promofarma\CartApi\Product\Domain\ProductRepository;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineRepository;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerNotFound;
use Promofarma\CartApi\Seller\Domain\SellerRepository;

class RemoveProductFromCartCommandHandlerTest extends TestCase
{
    private RemoveProductFromCartCommandHandler $handler;
    private $cartRepository;
    private $productRepository;
    private $productLineRepository;

    public function setUp(): void
    {
        $this->cartRepository = $this->createMock(CartRepository::class);
        $this->productRepository = $this->createMock(ProductRepository::class);
        $this->productLineRepository = $this->createMock(CartProductLineRepository::class);
        $this->handler = new RemoveProductFromCartCommandHandler($this->cartRepository, $this->productRepository, $this->productLineRepository);
    }

    /** @test */
    public function itShouldThrowAnExceptionWhenTheCartDoesNotExist()
    {
        $this->expectException(CartNotFound::class);
        $command = RemoveProductFromCartCommandMother::random();

        $this->cartRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(CartId::class))
            ->willThrowException(new CartNotFound());

        $this->handler->__invoke($command);
    }

    /** @test */
    public function itShouldThrowAnExceptionWhenTheProductDoesNotExist()
    {
        $this->expectException(ProductNotFound::class);
        $command = RemoveProductFromCartCommandMother::random();
        $cart = CartMother::random();

        $this->cartRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(CartId::class))
            ->willReturn($cart);

        $this->productRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(ProductId::class))
            ->willThrowException(new ProductNotFound());

        $this->handler->__invoke($command);
    }

    /** @test */
    public function itShouldRemoveAProductFromCart()
    {
        $product = ProductMother::random();
        $productLine = CartProductLineMother::random();
        $cart = CartMother::random();

        $this->cartRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(CartId::class))
            ->willReturn($cart);

        $this->productRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(ProductId::class))
            ->willReturn($product);

        $this->productLineRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(ProductId::class))
            ->willReturn($productLine);

        $deleteCommand = RemoveProductFromCartCommandMother::create(new CartId($cart->id()->value()), new ProductId($product->id()->value()));
        $this->handler->__invoke($deleteCommand);
    }
}
