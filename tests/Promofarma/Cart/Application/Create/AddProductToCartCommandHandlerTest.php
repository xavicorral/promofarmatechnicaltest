<?php

namespace App\Tests\Promofarma\Product\Application\Create;

use App\Tests\Promofarma\Cart\Application\Create\AddProductToCartCommandMother;
use App\Tests\Promofarma\Product\Domain\ProductMother;
use App\Tests\Promofarma\Seller\Domain\SellerMother;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Promofarma\CartApi\Cart\Application\Create\AddProductToCartCommandHandler;
use Promofarma\CartApi\Cart\Domain\Cart;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartRepository;
use Promofarma\CartApi\Cart\Domain\CartUserId;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Product\Domain\ProductNotFound;
use Promofarma\CartApi\Product\Domain\ProductRepository;
use Promofarma\CartApi\ProductLine\Domain\CartProductLine;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineRepository;

class AddProductToCartCommandHandlerTest extends TestCase
{
    private AddProductToCartCommandHandler $handler;
    private $productRepository;
    private $productLineRepository;
    private $cartRepository;

    public function setUp(): void
    {
        $this->productRepository = $this->createMock(ProductRepository::class);
        $this->cartRepository = $this->createMock(CartRepository::class);
        $this->productLineRepository = $this->createMock(CartProductLineRepository::class);
        $this->handler = new AddProductToCartCommandHandler($this->cartRepository, $this->productRepository, $this->productLineRepository);
    }

    /** @test */
    public function itShouldThrowAnExceptionIfUserIdIsNotValid()
    {
        $this->expectException(InvalidArgumentException::class);
        $cartUserId = new CartUserId('');
    }
    
    /** @test */
    public function itShouldThrowAnExceptionWhenTheProductDoesNotExist()
    {
        $command = AddProductToCartCommandMother::random();
        $this->expectException(ProductNotFound::class);

        $this->productRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(ProductId::class))
            ->willThrowException(new ProductNotFound());

        $this->handler->__invoke($command);
    }

    /** @test */
    public function itShouldCreateANewProductInANewCartWhenTheCartDoesNotExist()
    {
        $command = AddProductToCartCommandMother::random();
        $product = ProductMother::fromAddProductRequest($command);
        $seller = SellerMother::random();
        $product->addSeller($seller);

        $this->productRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(ProductId::class))
            ->willReturn($product);

        $this->cartRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(CartId::class))
            ->willReturn(null);

        $this->cartRepository
            ->expects($this->once())
            ->method('save')
            ->with($this->isInstanceOf(Cart::class))
            ->willReturn(null);

        $this->productLineRepository
            ->expects($this->once())
            ->method('save')
            ->with($this->isInstanceOf(CartProductLine::class))
            ->willReturn(null);

        $this->handler->__invoke($command);
    }
}
