<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Cart\Application\Create;

use App\Tests\Promofarma\Cart\Domain\CartIdMother;
use App\Tests\Promofarma\Cart\Domain\CartUserIdMother;
use App\Tests\Promofarma\Product\Domain\ProductIdMother;
use Promofarma\CartApi\Cart\Application\Create\AddProductToCartCommand;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartUserId;
use Promofarma\CartApi\Product\Domain\ProductId;

final class AddProductToCartCommandMother
{
    public static function create(CartId $id, CartUserId $userId, ProductId $productId): AddProductToCartCommand
    {
        return new AddProductToCartCommand($id->value(), $userId->value(), $productId->value());
    }

    public static function random(): AddProductToCartCommand
    {
        return self::create(CartIdMother::random(), CartUserIdMother::random(), ProductIdMother::random());
    }
}
