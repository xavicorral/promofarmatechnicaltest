<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Cart\Application\Amount;

use App\Tests\Promofarma\Cart\Domain\CartIdMother;
use App\Tests\Promofarma\Product\Domain\ProductIdMother;
use Promofarma\CartApi\Cart\Application\Amount\ModifyProductLineAmountCommand;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Product\Domain\ProductId;

final class ModifyProductLineAmountCommandMother
{
    public static function create(CartId $cartId, ProductId $productId, string $operation): ModifyProductLineAmountCommand
    {
        return new ModifyProductLineAmountCommand($cartId->value(), $productId->value(), $operation);
    }

    public static function random(): ModifyProductLineAmountCommand
    {
        return self::create(CartIdMother::random(), ProductIdMother::random(), 'increase');
    }
}
