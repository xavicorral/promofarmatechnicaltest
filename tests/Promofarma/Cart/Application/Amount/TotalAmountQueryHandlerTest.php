<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Cart\Application\Amount;

use App\Tests\Promofarma\Cart\Domain\CartMother;
use App\Tests\Promofarma\ProductLine\Domain\CartProductLineMother;
use PHPUnit\Framework\TestCase;
use Promofarma\CartApi\Cart\Application\Amount\TotalAmountQueryHandler;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartNotFound;
use Promofarma\CartApi\Cart\Domain\CartRepository;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineRepository;

final class TotalAmountQueryHandlerTest extends TestCase
{
    private TotalAmountQueryHandler $handler;
    private $cartRepository;
    private $productLineRepository;

    public function setUp(): void
    {
        $this->cartRepository = $this->createMock(CartRepository::class);
        $this->productLineRepository = $this->createMock(CartProductLineRepository::class);
        $this->handler = new TotalAmountQueryHandler($this->cartRepository, $this->productLineRepository);
    }

    /** @test */
    public function itShouldThrowAnExceptionWhenTheCartDoesNotExist()
    {
        $query = TotalAmountQueryMother::random();
        $this->expectException(CartNotFound::class);

        $this->cartRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(CartId::class))
            ->willThrowException(new CartNotFound());

        $this->handler->__invoke($query);
    }

    /** @test */
    public function itShouldReturnAZeroAmount()
    {
        $query = TotalAmountQueryMother::random();
        $cart = CartMother::random();

        $this->cartRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(CartId::class))
            ->willReturn($cart);

        $this->productLineRepository
            ->expects($this->once())
            ->method('findAllProductLinesByCartId')
            ->with($this->isInstanceOf(CartId::class))
            ->willReturn($cart);

        $totalAmount = $this->handler->__invoke($query);
        $this->assertEquals(0, $totalAmount);
    }

    /** @test */
    public function itShouldReturnATwoAmount()
    {
        $query = TotalAmountQueryMother::random();
        $cart = CartMother::random();
        $cart->addProductLine(CartProductLineMother::random());
        $cart->addProductLine(CartProductLineMother::random());

        $this->cartRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(CartId::class))
            ->willReturn($cart);

        $this->productLineRepository
            ->expects($this->once())
            ->method('findAllProductLinesByCartId')
            ->with($this->isInstanceOf(CartId::class))
            ->willReturn($cart->product_lines());

        $totalAmount = $this->handler->__invoke($query);
        $this->assertEquals(2, $totalAmount);
    }
}
