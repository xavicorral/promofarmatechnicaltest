<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Cart\Application\Amount;

use App\Tests\Promofarma\Cart\Domain\CartIdMother;
use Promofarma\CartApi\Cart\Application\Amount\TotalAmountQuery;
use Promofarma\CartApi\Cart\Domain\CartId;

final class TotalAmountQueryMother
{
    public static function create(CartId $cartId): TotalAmountQuery
    {
        return new TotalAmountQuery($cartId);
    }

    public static function random(): TotalAmountQuery
    {
        return self::create(CartIdMother::random());
    }
}
