<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Cart\Application\Amount;

use App\Tests\Promofarma\Cart\Domain\CartMother;

use PHPUnit\Framework\TestCase;
use Promofarma\CartApi\Cart\Application\Amount\ModifyProductLineAmountCommandHandler;
use Promofarma\CartApi\Cart\Domain\CartId;
use Promofarma\CartApi\Cart\Domain\CartNotFound;
use Promofarma\CartApi\Cart\Domain\CartRepository;
use Promofarma\CartApi\ProductLine\Domain\CartProductLine;
use Promofarma\CartApi\ProductLine\Domain\CartProductLineRepository;

final class ModifyProductLineAmountCommandHandlerTest extends TestCase
{
    private ModifyProductLineAmountCommandHandler $handler;
    private $cartRepository;
    private $productLineRepository;

    public function setUp(): void
    {
        $this->cartRepository = $this->createMock(CartRepository::class);
        $this->productLineRepository = $this->createMock(CartProductLineRepository::class);
        $this->handler = new ModifyProductLineAmountCommandHandler($this->cartRepository, $this->productLineRepository);
    }

    /** @test */
    public function itShouldThrowAnExceptionWhenTheCartDoesNotExist()
    {
        $this->expectException(CartNotFound::class);
        $command = ModifyProductLineAmountCommandMother::random();

        $this->cartRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(CartId::class))
            ->willThrowException(new CartNotFound());

        $this->handler->__invoke($command);
    }

    /** @test */
    public function itShouldIncreaseTheAmountOfAProductLine()
    {
        $command = ModifyProductLineAmountCommandMother::random();
        $cart = CartMother::random();
        //$productLine = CartProductLineMother::random();
        $productLineMock = $this->createMock(CartProductLine::class);

        $this->cartRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(CartId::class))
            ->willReturn($cart);

        $this->productLineRepository
            ->expects($this->once())
            ->method('findOneProductLineByCartIdAndProductId')
            ->with($this->isInstanceOf(CartId::class))
            ->willReturn($productLineMock);

        $this->productLineRepository
            ->expects($this->once())
            ->method('save')
            ->with($this->isInstanceOf(CartProductLine::class))
            ->willReturn(null);

        $this->handler->__invoke($command);
    }
}
