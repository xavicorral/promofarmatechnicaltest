<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Product\Domain;

use App\Tests\Promofarma\Shared\Domain\WordMother;
use Promofarma\CartApi\Product\Domain\ProductName;

final class ProductNameMother
{
    public static function create(string $value): ProductName
    {
        return new ProductName($value);
    }

    public static function random(): ProductName
    {
        return self::create(WordMother::random());
    }
}
