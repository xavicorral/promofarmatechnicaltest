<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Product\Domain;

use App\Tests\Promofarma\Shared\Domain\UuidMother;
use Promofarma\CartApi\Product\Domain\ProductId;

final class ProductIdMother
{
    public static function create(string $value): ProductId
    {
        return new ProductId($value);
    }

    public static function random(): ProductId
    {
        return self::create(UuidMother::random());
    }
}
