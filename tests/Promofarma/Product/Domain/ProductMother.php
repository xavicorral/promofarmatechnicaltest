<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Product\Domain;

use App\Tests\Promofarma\Cart\Domain\CartIdMother;
use App\Tests\Promofarma\Seller\Domain\SellerMother;
use Promofarma\CartApi\Cart\Application\Create\AddProductToCartCommand;
use Promofarma\CartApi\Product\Application\Create\CreateProductCommand;
use Promofarma\CartApi\Product\Domain\Product;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Product\Domain\ProductName;
use Promofarma\CartApi\Seller\Domain\Seller;

final class ProductMother
{
    public static function create(ProductId $id, ProductName $name, Seller $seller): Product
    {
        $product = Product::create($id, $name);
        $product->addSeller($seller);

        return $product;
    }

    public static function fromRequest(CreateProductCommand $request): Product
    {
        return self::create(
            ProductIdMother::create($request->uuid()),
            ProductNameMother::create($request->name()),
            SellerMother::random()
        );
    }

    public static function fromAddProductRequest(AddProductToCartCommand $request): Product
    {
        return self::create(
            ProductIdMother::create($request->productUuid()),
            ProductNameMother::random(),
            SellerMother::random()
        );
    }

    public static function random(): Product
    {
        return self::create(ProductIdMother::random(), ProductNameMother::random(), SellerMother::random());
    }
}
