<?php

namespace App\Tests\Promofarma\Product\Application\Create;

use App\Tests\Promofarma\Product\Domain\ProductMother;
use App\Tests\Promofarma\Seller\Domain\SellerMother;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Promofarma\CartApi\Product\Application\Create\CreateProductCommandHandler;
use Promofarma\CartApi\Product\Domain\Product;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Product\Domain\ProductName;
use Promofarma\CartApi\Product\Domain\ProductRepository;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerNotFound;
use Promofarma\CartApi\Seller\Domain\SellerRepository;

class CreateProductCommandHandlerTest extends TestCase
{
    private CreateProductCommandHandler $handler;
    private $productRepository;
    private $sellerRepository;

    public function setUp(): void
    {
        $this->productRepository = $this->createMock(ProductRepository::class);
        $this->sellerRepository = $this->createMock(SellerRepository::class);
        $this->handler = new CreateProductCommandHandler($this->productRepository, $this->sellerRepository);
    }

    /** @test */
    public function itShouldThrowAnExceptionIfProductNameIsEmpty()
    {
        $this->expectException(InvalidArgumentException::class);
        $productName = new ProductName('');
    }
    
    /** @test */
    public function itShouldThrowAnExceptionIfProductUuidIsNotValid()
    {
        $this->expectException(InvalidArgumentException::class);
        $productId = new ProductId('3fe1bce8-7282-467a-872fd7-b876fe97ad4b');
    }


    /** @test */
    public function itShouldThrowAnExceptionWhenTheSellerDoesNotExist()
    {
        $command = CreateProductCommandMother::random();
        $this->expectException(SellerNotFound::class);

        $this->sellerRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(SellerId::class))
            ->willThrowException(new SellerNotFound());

        $this->handler->__invoke($command);
    }

    /** @test */
    public function itShouldCreateANewProduct()
    {
        $command = CreateProductCommandMother::random();
        $product = ProductMother::fromRequest($command);
        $seller = SellerMother::random();
        $product->addSeller($seller);

        $this->sellerRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(SellerId::class))
            ->willReturn($seller);

        $this->productRepository
            ->expects($this->once())
            ->method('save')
            ->with($this->isInstanceOf(Product::class))
            ->willReturn(null);

        $this->handler->__invoke($command);
    }
}
