<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Product\Application\Create;

use App\Tests\Promofarma\Product\Domain\CartIdMother;
use App\Tests\Promofarma\Product\Domain\ProductIdMother;
use App\Tests\Promofarma\Product\Domain\ProductNameMother;
use App\Tests\Promofarma\Seller\Domain\SellerIdMother;
use Promofarma\CartApi\Product\Application\Create\CreateProductCommand;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Product\Domain\ProductName;
use Promofarma\CartApi\Seller\Domain\SellerId;

final class CreateProductCommandMother
{
    public static function create(ProductId $id, ProductName $name, SellerId $sellerId): CreateProductCommand
    {
        return new CreateProductCommand($id->value(), $name->value(), $sellerId->value());
    }

    public static function random(): CreateProductCommand
    {
        return self::create(ProductIdMother::random(), ProductNameMother::random(), SellerIdMother::random());
    }
}
