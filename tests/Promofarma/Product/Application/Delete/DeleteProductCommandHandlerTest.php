<?php

namespace App\Tests\Promofarma\Product\Application\Create;

use App\Tests\Promofarma\Product\Application\Delete\DeleteProductCommandMother;
use App\Tests\Promofarma\Product\Application\Delete\RemoveProductFromCartCommandMother;
use App\Tests\Promofarma\Product\Domain\ProductMother;
use App\Tests\Promofarma\Seller\Domain\SellerMother;
use PHPUnit\Framework\TestCase;
use Promofarma\CartApi\Product\Application\Create\CreateProductCommandHandler;
use Promofarma\CartApi\Product\Application\Delete\DeleteProductCommandHandler;
use Promofarma\CartApi\Product\Domain\Product;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Product\Domain\ProductName;
use Promofarma\CartApi\Product\Domain\ProductNotFound;
use Promofarma\CartApi\Product\Domain\ProductRepository;
use Promofarma\CartApi\Product\Infrastructure\Persistence\InMemoryProductRepository;
use Promofarma\CartApi\Seller\Domain\SellerId;
use Promofarma\CartApi\Seller\Domain\SellerNotFound;
use Promofarma\CartApi\Seller\Domain\SellerRepository;

class DeleteProductCommandHandlerTest extends TestCase
{
    private DeleteProductCommandHandler $handler;
    private $productRepository;
    private $sellerRepository;

    public function setUp(): void
    {
        $this->productRepository = $this->createMock(ProductRepository::class);
        $this->sellerRepository = $this->createMock(SellerRepository::class);
        $this->handler = new DeleteProductCommandHandler($this->productRepository, $this->sellerRepository);
    }

    /** @test */
    public function itShouldThrowAnExceptionWhenTheSellerDoesNotExist()
    {
        $this->expectException(SellerNotFound::class);
        $command = DeleteProductCommandMother::random();

        $this->sellerRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(SellerId::class))
            ->willThrowException(new SellerNotFound());

        $this->handler->__invoke($command);
    }

    /** @test */
    public function itShouldThrowAnExceptionWhenTheProductDoesNotExist()
    {
        $this->expectException(ProductNotFound::class);
        $command = DeleteProductCommandMother::random();
        $seller = SellerMother::random();

        $this->sellerRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(SellerId::class))
            ->willReturn($seller);

        $this->productRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(ProductId::class))
            ->willThrowException(new ProductNotFound());

        $this->handler->__invoke($command);
    }

    /** @test */
    public function itShouldDeleteAProduct()
    {
        $product = ProductMother::random();
        $seller = SellerMother::random();
        $product->addSeller($seller);

        $this->sellerRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(SellerId::class))
            ->willReturn($seller);

        $this->productRepository
            ->expects($this->once())
            ->method('find')
            ->with($this->isInstanceOf(ProductId::class))
            ->willReturn($product);

        $deleteCommand = DeleteProductCommandMother::create(new ProductId($product->id()->value()), new SellerId($seller->id()->value()));
        $this->handler->__invoke($deleteCommand);
    }
}
