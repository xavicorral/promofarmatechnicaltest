<?php

declare(strict_types=1);

namespace App\Tests\Promofarma\Product\Application\Delete;

use App\Tests\Promofarma\Cart\Domain\CartIdMother;
use App\Tests\Promofarma\Product\Domain\ProductIdMother;
use App\Tests\Promofarma\Seller\Domain\SellerIdMother;
use Promofarma\CartApi\Product\Application\Delete\DeleteProductCommand;
use Promofarma\CartApi\Product\Domain\ProductId;
use Promofarma\CartApi\Seller\Domain\SellerId;

final class DeleteProductCommandMother
{
    public static function create(ProductId $id, SellerId $selledId): DeleteProductCommand
    {
        return new DeleteProductCommand($id->value(), $selledId->value());
    }

    public static function random(): DeleteProductCommand
    {
        return self::create(ProductIdMother::random(), SellerIdMother::random());
    }
}
