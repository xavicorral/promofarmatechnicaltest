<?php

declare(strict_types = 1);

namespace App\Tests\Promofarma\Shared\Domain;

use App\Tests\Promofarma\Shared\Domain\MotherCreator;

final class WordMother
{
    public static function random(): string
    {
        return MotherCreator::random()->word;
    }
}
