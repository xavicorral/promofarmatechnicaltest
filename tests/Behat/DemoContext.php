<?php

declare(strict_types=1);

namespace App\Tests\Behat;

use Behat\Behat\Context\Context;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
final class DemoContext implements Context
{
    /** @var KernelInterface */
    private $kernel;

    /** @var Response|null */
    private $response;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @BeforeScenario
     */
    public function clearData()
    {
        $purger = new ORMPurger($this->kernel->getContainer()->get('doctrine')->getManager());
        $purger->purge();
    }

    /**
     * @When I send a request with method :method to :path
     */
    public function iSendARequestWithMethodTo(string $path, string $method): void
    {
        $this->response = $this->kernel->handle(Request::create($path, $method));
    }

    /**
     * @When I send a request with method :method to :path with params:
     */
    public function iSendARequestWithMethodToPathWithBody(string $path, string $method, string $params): void
    {
        $this->response = $this->kernel->handle(Request::create($path, $method, $params));
    }

    /**
     * @Then the response should be received
     */
    public function theResponseShouldBeReceived(): void
    {
        if ($this->response === null) {
            throw new \RuntimeException('No response received');
        }
    }

    /**
     * @Then the response content should be:
     */
    public function theResponseContentShouldBe($expectedResponse): void
    {
        if ($this->response->getContent() !== $expectedResponse->getRaw()) {
            throw new RuntimeException(
                sprintf("The outputs does not match!\n\n-- Expected:\n%s\n\n-- Actual:\n%s", $expectedResponse->getRaw(), $this->response->getContent())
            );
        }
    }

    /**
     * @Then the response should be empty
     */
    public function theResponseShouldBeEmpty(): void
    {
        if (!empty($this->response->getContent())) {
            throw new RuntimeException(
                sprintf("The outputs is not empty, Actual:\n%s", $this->response->getContent())
            );
        }
    }

    /**
     * @Then the response status code should be :expectedResponseCode
     */
    public function theResponseStatusCodeShouldBe($expectedResponseCode): void
    {
        if ($this->response->getStatusCode() !== (int) $expectedResponseCode) {
            throw new RuntimeException(
                sprintf(
                    'The status code <%s> does not match the expected <%s>',
                    $this->response->getStatusCode(),
                    $expectedResponseCode
                )
            );
        }
    }
}
