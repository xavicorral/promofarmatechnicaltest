
test:
	@docker exec promofarma_php_fpm  make run-tests

run-tests:
	./bin/phpunit --exclude-group='disabled' tests
	./vendor/bin/behat --format=progress -v


start-local-api:
	symfony serve --dir=public --port=8000 -d --no-tls --force-php-discovery

stop-local-api:
	symfony server:stop --dir=public