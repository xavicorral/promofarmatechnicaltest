# Xavi Corral Technical Test

Promofarma Shopping Cart & Orders

[![pipeline status](https://gitlab.com/xavicorral/promofarmatechnicaltest/badges/master/pipeline.svg)](https://gitlab.com/xavicorral/promofarmatechnicaltest/-/commits/master)

Promofarma is a marketplace that operates with different sellers that offer their products to be sold from Promofarma. 
This implies that a product can be sold by one or several sellers at different prices. 

Therefore, in our system we have to trace the selection of the product-seller once the user adds a product to the Shopping Cart and once the transaction ends.

## Implement Shopping Cart API:

## 🚀 Environment setup

### 🐳 Needed tools

1. [Install Docker](https://www.docker.com/get-started)
2. [Install Composer](https://getcomposer.org/download/)
2. Clone this project: `git clone https://gitlab.com/xavicorral/promofarmatechnicaltest xcorral_technical_test`
3. Move to the project folder: `cd xcorral_technical_test`
4. Install dependencies: `composer install` 

### 🌍 Application execution

1. Bring up the project Docker containers with Docker Compose: `docker-compose up -d`
2. Go to [the API health check page](http://localhost:8000/api/health-check)

Database tables should be created the first time the mysql container is created. In case of troubles, you may find a snapshot of the structure at /database/init.sql
### ✅ Tests execution

Execute Behat and PHP Unit tests: `make test`

### Cart API Description

The base URL for accessing the API is http://localhost:8000/api. As an example: http://localhost:8000/api/health-check  

Starting from that url, the following methods with its actions have been implemented:

| METHOD   |      URL      |  Params | Description |
|:----------:|:-------------:|:------:|:------:|
| GET |  /health-check | None | Check the status of the Api |
| POST | /seller | uuid (seller Uuid), name (seller Name)  | Create a new Seller |
| DELETE | /seller | uuid (seller Uuid) | Delete a given seller |
| POST | /seller/{uuid}/product | uuid (product Uuid), name (product Name) | Create a product linked to a given seller |
| DELETE | /seller/{uuid}/product | uuid (product Uuid) | Delete a product from a given seller |
| POST | /cart/{uuid}/product | user_id (user Uuid), product_id (product Uuid) | Add a product to a Cart. If the cart does not exist, it will be created |
| DELETE | /cart/{uuid}/product | product_id (product Uuid) | Remove a product from a given cart |
| GET |  /cart/{uuid}/amount | None | Returns the total amount of items in a Cart |
| PUT |  /cart/{uuid}/amountModify/{productUuid}/increase | None | Increases the amount of a given product in a Cart |
| PUT |  /cart/{uuid}/amountModify/{productUuid}/decrease | None | Decreases the amount of a given product in a Cart |
| DELETE |  /cart/{uuid} | None | Deletes a Cart and its products |
| PUT |  /cart/{uuid}/checkout | None | Marks a Cart as checked-out |

If you want to try each one of there methods manually, you can open this file:

`postman/Promofarma.postman_collection.json`

with Postman (https://www.postman.com/)