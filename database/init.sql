
use promofarma;

# Volcado de tabla cart
# ------------------------------------------------------------

CREATE TABLE `cart` (
  `cart_uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`cart_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla cart_product_lines
# ------------------------------------------------------------

CREATE TABLE `cart_product_lines` (
  `product_line_uuid_product_line_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart_uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`product_line_uuid_product_line_id`,`cart_uuid`,`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla products
# ------------------------------------------------------------

CREATE TABLE `products` (
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla sellers
# ------------------------------------------------------------

CREATE TABLE `sellers` (
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla sellers_products
# ------------------------------------------------------------

CREATE TABLE `sellers_products` (
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seller_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`product_id`,`seller_id`),
  KEY `IDX_297F0F524584665A` (`product_id`),
  KEY `IDX_297F0F528DE820D9` (`seller_id`),
  CONSTRAINT `FK_297F0F524584665A` FOREIGN KEY (`product_id`) REFERENCES `products` (`uuid`),
  CONSTRAINT `FK_297F0F528DE820D9` FOREIGN KEY (`seller_id`) REFERENCES `sellers` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


